﻿namespace HibernationPreventer.Task
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using Domain.Entities;
	using Microsoft.Win32.TaskScheduler;
	using Service;

	public class TaskRunner
	{
		private const string TaskName = "WakeFromHibernation";
		private static ApplicationService _applicationService;

		private static bool _inRunningState;
		private static uint _originalState;

		public TaskRunner(ApplicationService applicationService)
		{
			_applicationService = applicationService;
		}

		#region Methods

		public static void SetupTask()
		{
			using (TaskService ts = new TaskService())
			{
				Task t = ts.FindTask(TaskName);
				if (t == null)
				{
					t = ts.AddTask(
							TaskName,
							new TimeTrigger {StartBoundary = DateTime.Now + TimeSpan.FromHours(2), Enabled = true},
							new ExecAction(@"C:\Share\HibernationPreventer\RestartService.bat"));

					t.Definition.Settings.WakeToRun = true;
					t.Definition.Principal.UserId = "SYSTEM";
					//t.Definition.Principal.LogonType = TaskLogonType.;
					t.RegisterChanges();
				}
				else
				{
					(t.Definition.Triggers.First() as TimeTrigger).StartBoundary = DateTime.Now + TimeSpan.FromHours(2);
					t.RegisterChanges();
				}
			}
		}

		public static void Run()
		{
			List<Application> applications = _applicationService.SelectAll().ToList();

			bool isRunning =
					applications.Select(application => Process.GetProcessesByName(application.Name)).
								Any(processes => processes.Length > 0);

			if (isRunning)
			{
				if (!_inRunningState)
				{
					_originalState =
							NativeMethods.SetThreadExecutionState(
									NativeMethods.ES_CONTINUOUS | NativeMethods.ES_SYSTEM_REQUIRED);
					_inRunningState = true;
				}

				// CREATE TASK IN TASK MANAGER
				using (TaskService ts = new TaskService())
				{
					Task t = ts.FindTask(TaskName);
					if (t == null)
					{
						t = ts.AddTask(
								TaskName,
								new TimeTrigger {StartBoundary = DateTime.Now + TimeSpan.FromHours(2), Enabled = true},
								new ExecAction(@"C:\Share\HibernationPreventer\RestartService.bat"));

						t.Definition.Settings.WakeToRun = true;
						t.Definition.Principal.UserId = "SYSTEM";
						t.RegisterChanges();
					}
					else
					{
						(t.Definition.Triggers.First() as TimeTrigger)..StartBoundary = DateTime.Now + TimeSpan.FromHours(2);
						t.RegisterChanges();
					}
				}
			}
			else
			{
				_inRunningState = false;
				NativeMethods.SetThreadExecutionState(_originalState);
			}
		}

		#endregion
	}
}