﻿namespace HibernationPreventer.Domain.Repositories
{
    using System.Collections.Generic;
    using System.Linq;

    public interface IRepository<T> where T : class
    {
        #region Methods

        IQueryable<T> GetAll();
        T GetById(object id);
        void Add(T entity);
        void Update(T entity);
        void Delete(object id);
        void Delete(T entity);
        IEnumerable<T> GetWithRawSql(string query, params object[] parameters);

        #endregion
    }
}