﻿namespace HibernationPreventer.Domain.Repositories
{
    using Entities;

    public interface IUoW
    {
        IRepository<Application> Applications { get; }

        #region Methods

        void Commit();

        #endregion
    }
}