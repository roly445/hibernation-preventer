﻿namespace HibernationPreventer.Repository
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Diagnostics;
    using Domain.Entities;

    public class RepoDbContext : DbContext
    {
        //public RepoDbContext() : base("ConnStr") {}
        public RepoDbContext()
            : base("ConnStr")
        {
            Debug.Write(Database.Connection.ConnectionString);
        }

        public DbSet<Application> Applications { get; set; }

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Application>().Property(a => a.Name).HasMaxLength(260);
        }

        #endregion
    }
}