﻿namespace HibernationPreventer.Harness
{
    using System;
    using System.Timers;
    using Domain.Repositories;
    using Ninject;
    using Repository;
    using Service;
    using Task;
    using TrekkingforCharity.Web.Repository;

    internal class Program
    {
        private static Timer _timer;

        #region Methods

        private static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<ApplicationService>().To<ApplicationService>();
            kernel.Bind<TaskRunner>().To<TaskRunner>().InSingletonScope();
            kernel.Bind<IUoW>().To<UoW>();
            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();

            TaskRunner task = kernel.Get<TaskRunner>();

            using (_timer = new Timer(6000))
            {
                _timer.Elapsed += TimerElapsed;
                _timer.Start();
                while (true)
                {
                    if (Console.ReadLine() == "quit")
                    {
                        break;
                    }
                }
            }
        }

        private static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            TaskRunner.Run();
            _timer.Start();
        }

        #endregion
    }
}