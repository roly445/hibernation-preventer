namespace HibernationPreventer.Site.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ApplicationCreateModel : BaseModel
    {
        [Required]
        public string Name { get; set; }
    }
}