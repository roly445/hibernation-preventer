namespace HibernationPreventer.Site.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Domain.Entities;

    public class ApplicationListingModel : BaseModel
    {
        public ApplicationListingModel()
        {
            Applications = new Collection<Application>();
        }

        public ICollection<Application> Applications { get; set; }
        public int ApplicationToDelete { get; set; }
    }
}