﻿namespace HibernationPreventer.Site.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Entities;
    using Models;
    using Service;

    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private readonly ApplicationService _applicationService;

        public HomeController(ApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        #region Index

        public ActionResult Index()
        {
            return View(CreateApplicationListingModel());
        }

        [HttpPost]
        public ActionResult Index(ApplicationListingModel applicationListingViewModel)
        {
            if (ModelState.IsValid)
            {
                _applicationService.Delete(applicationListingViewModel.ApplicationToDelete);
            }

            return View(
                CreateApplicationListingModel(applicationListingViewModel));
        }

        private ApplicationListingModel CreateApplicationListingModel(
            ApplicationListingModel applicationListingModel = null)
        {
            if (applicationListingModel == null)
                applicationListingModel = new ApplicationListingModel();

            applicationListingModel.Applications = _applicationService.SelectAll().ToList();

            return applicationListingModel;
        }

        #endregion Index

        #region Add Application

        public ActionResult AddApplication()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddApplication(ApplicationCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _applicationService.Create(
                    new Application
                    {
                        Name = model.Name
                    });

                return RedirectToAction("Index");
            }
            return View(model);
        }

        #endregion Add Application
    }
}