﻿namespace HibernationPreventer.WindowsService
{
	using System.ServiceProcess;
	using System.Timers;
	using Domain.Repositories;
	using Microsoft.Win32;
	using Ninject;
	using Repository;
	using Service;
	using Task;
	using TrekkingforCharity.Web.Repository;

	public partial class Service1 : ServiceBase
	{
		private Timer _intervalTimer;

		public Service1()
		{
			InitializeComponent();
			IKernel kernel = new StandardKernel();
			kernel.Bind<ApplicationService>().ToSelf();
			kernel.Bind<TaskRunner>().ToSelf().InSingletonScope();
			kernel.Bind<IUoW>().To<UoW>();
			kernel.Bind<RepositoryFactories>().ToSelf().InSingletonScope();
			kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();

			SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(SystemEvents_PowerModeChanged);

			_intervalTimer = new Timer(); // Default in case app.config is silent.
			_intervalTimer.Enabled = true;
			_intervalTimer.Elapsed += new ElapsedEventHandler(this.IntervalTimer_Elapsed);

#if DEBUG
			_intervalTimer.Interval = 1000;
#else
			_intervalTimer.Interval = 60000;
#endif
		}

		private void IntervalTimer_Elapsed(object sender, ElapsedEventArgs e)
		{
			_intervalTimer.Stop();
			TaskRunner.Run();
#if DEBUG
			_intervalTimer.Interval = 1000;
#else
			_intervalTimer.Interval = 60000;
#endif
			_intervalTimer.Start();
		}

		public void Process()
		{
			OnStart(null);
		}

		private void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
		{
			switch (e.Mode)
			{
				case PowerModes.Resume:
					TaskRunner.SetupTask();
					break;
			}
		}

		#region Methods

		protected override void OnStart(string[] args)
		{
			TaskRunner.SetupTask();
			_intervalTimer.Start();
		}

		protected override void OnStop()
		{
			_intervalTimer.Stop();
		}

		#endregion
	}
}