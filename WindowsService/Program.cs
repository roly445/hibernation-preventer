﻿namespace WindowsService
{
	using System;
	using System.ServiceProcess;
	using HibernationPreventer.WindowsService;

	internal static class Program
	{
		#region Methods

		/// <summary>
		///     The main entry point for the application.
		/// </summary>
		private static void Main()
		{
			//ServiceBase[] ServicesToRun;
			//ServicesToRun = new ServiceBase[]
			//{
			//	new Service1()
			//};
			//ServiceBase.Run(ServicesToRun);

#if(!DEBUG)
			ServiceBase[] ServicesToRun;
           ServicesToRun = new ServiceBase[] 
	   { 
	        new Service1() 
	   };
           ServiceBase.Run(ServicesToRun);
#else
			Service1 myServ = new Service1();
			myServ.Process();

			while (true)
			{
				if (Console.ReadLine() == "quit")
				{
					break;
				}
			}

			// here Process is my Service function
			// that will run when my service onstart is call
			// you need to call your own method or function name here instead of Process();
#endif
		}

		#endregion
	}
}