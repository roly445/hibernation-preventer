﻿namespace HibernationPreventer.Service
{
    using System.Linq;
    using Domain.Entities;
    using Domain.Repositories;

    public class ApplicationService
    {
        private readonly IUoW _uow;

        public ApplicationService(IUoW uow)
        {
            _uow = uow;
        }

        #region Methods

        public Application Create(Application application)
        {
            _uow.Applications.Add(application);
            _uow.Commit();
            return application;
        }

        public Application Update(Application application)
        {
            _uow.Applications.Update(application);
            _uow.Commit();
            return application;
        }

        public void Delete(Application application)
        {
            _uow.Applications.Delete(application);
            _uow.Commit();
        }

        public void Delete(int id)
        {
            _uow.Applications.Delete(id);
            _uow.Commit();
        }

        public IQueryable<Application> SelectAll()
        {
            return _uow.Applications.GetAll();
        }

        public Application SelectById(int id)
        {
            return _uow.Applications.GetById(id);
        }

        #endregion
    }
}